const gulp = require('gulp'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('serve', ['watch'], () => {
  connect.server({
    root: paths.prod.target,
    livereload: true
  });
});