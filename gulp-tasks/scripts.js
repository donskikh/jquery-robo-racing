const gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    paths = require('./paths.json');

gulp.task('scripts', ['eslint'], () => {
    return gulp.src(paths.dev.scripts)
        .pipe(plugins.newer(paths.prod.scriptsTarget))
        .pipe(gulp.dest(paths.prod.scriptsTarget))
        .pipe(plugins.connect.reload());
});