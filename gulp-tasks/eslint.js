const gulp = require('gulp'),
    eslint = require('gulp-eslint'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

gulp.task('eslint', function() {
    return gulp.src(paths.dev.scriptsNoVendor)
        .pipe(newer(paths.prod.scriptsTarget))
        .pipe(eslint())
        .pipe(eslint.format());
});