const gulp = require('gulp'),
    connect = require('gulp-connect'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

gulp.task('html', () => {
    return gulp.src(paths.dev.html)
        .pipe(newer(paths.prod.target))
        .pipe(gulp.dest(paths.prod.target))
        .pipe(connect.reload());
});