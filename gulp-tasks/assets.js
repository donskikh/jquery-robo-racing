const gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    lazypipe = require('lazypipe'),
    argv = require('yargs').argv,
    autoprefixer = require('autoprefixer'),
    autoprefixerLast2Versions = plugins.postcss([autoprefixer({
        browsers: ['last 2 versions']
    })]),
    paths = require('./paths.json');

gulp.task('assets', ['images', 'sounds', 'eslint'], () => {
    const indexHtmlFilter = plugins.filter(['**/*', '!**/index.html'], {
        restore: true
    });

    if (argv.production) {
        return gulp.src(paths.dev.html)
            .pipe(plugins.useref({}, lazypipe().pipe(plugins.sourcemaps.init, {
                loadMaps: true
            })))
            .pipe(plugins.newer(paths.prod.target))
            .pipe(indexHtmlFilter)
            .pipe(plugins.rev())
            .pipe(indexHtmlFilter.restore)
            .pipe(plugins.revReplace())
            .pipe(plugins.if('*.js', plugins.uglify()))
            .pipe(plugins.if('*.css', autoprefixerLast2Versions))
            .pipe(plugins.if('*.css', plugins.cleanCss()))
            .pipe(plugins.sourcemaps.write())
            .pipe(gulp.dest(paths.prod.target))
    } else {
        return gulp.src(paths.dev.html)
            .pipe(plugins.useref({
                noconcat: true
            }))
            .pipe(plugins.newer(paths.prod.target))
            .pipe(plugins.if('*.css', autoprefixerLast2Versions))
            .pipe(gulp.dest(paths.prod.target))
    }
});