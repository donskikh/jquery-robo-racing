const gulp = require('gulp'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

gulp.task('sounds', () => {
    gulp.src(paths.dev.sounds)
        .pipe(newer(paths.prod.soundsTarget))
        .pipe(gulp.dest(paths.prod.soundsTarget));
});