const gulp = require('gulp'),
    runSequence = require('run-sequence'),
    argv = require('yargs').argv;

require('gulp-stats')(gulp);

gulp.task('build', () => {

    if (argv.production) {
        runSequence('clean', ['assets', 'documentation']);
    } else {
        gulp.start('assets');
    }
    if (!argv.skiptests) {
        gulp.start('test');
    }
});