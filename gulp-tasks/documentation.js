const gulp = require('gulp'),
    documentation = require('gulp-documentation'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

gulp.task('documentation', () => {
    return gulp.src(paths.dev.scriptsNoVendor)
        .pipe(newer(paths.documentation.html))
        .pipe(documentation({ format: 'html' }))
        .pipe(gulp.dest(paths.documentation.html));
});