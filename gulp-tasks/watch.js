const gulp = require('gulp'),
    paths = require('./paths.json');

gulp.task('watch', ['build'], () => {
    gulp.watch(paths.dev.styles, ['styles']);
    gulp.watch(paths.dev.scripts, ['scripts']);
    gulp.watch(paths.dev.html, ['html']);
});