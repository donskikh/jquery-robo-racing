const gulp = require('gulp'),
    imageMin = require('gulp-imagemin'),
    jpegtran = require('imagemin-jpegtran'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

gulp.task('images', () => {
    return gulp.src(paths.dev.images)
        .pipe(newer(paths.prod.imagesTarget))
        .pipe(imageMin({ progressive: true, use: [jpegtran()] }))
        .pipe(gulp.dest(paths.prod.imagesTarget));
});