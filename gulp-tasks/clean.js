const gulp = require('gulp'),
    clean = require('gulp-clean'),
    paths = require('./paths.json');

gulp.task('clean', () => {
    return gulp.src([paths.prod.target, paths.documentation.html], {read: false})
        .pipe(clean({force: true}));
});