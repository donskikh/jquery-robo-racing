const gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    autoprefixer = require('autoprefixer'),
    paths = require('./paths.json');

gulp.task('styles', () => {
    return gulp.src(paths.dev.styles)
        .pipe(plugins.newer(paths.prod.stylesTarget))
        .pipe(plugins.postcss([autoprefixer({
            browsers: ['last 2 versions']
        })]))
        .pipe(gulp.dest(paths.prod.stylesTarget))
        .pipe(plugins.connect.reload());
});