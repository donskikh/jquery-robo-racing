describe('Utils module', function() {

    var specialTestValues = [null, undefined, {}],
        arrayTestValue = [['test']],
        numberTestValue = [Math.PI],
        booleanTestValue = [true];

    function generateTestsWithBadParameters(testValuesArray, targetTypeName, action) {
        var testedTypeName;

        testValuesArray.forEach(function(elem) {
            testedTypeName = getTypeName(elem);

            var testDescription = 'should return false if ' + testedTypeName + ' was passed instead of a ' + targetTypeName;

            it(testDescription, function() {
                expect(action(elem)).toBeFalsy();
            });
        });
    }

    function getTypeName(type) {
        var typeName;

        if (type instanceof Array) {
            typeName = 'array'
        } else if (type === null) {
            typeName = 'null';
        } else if (type === undefined) {
            typeName = 'undefined';
        } else {
            typeName = typeof type;
        }

        return typeName;
    }

    describe('isStringNotEmpty()', function() {
        var testValues = specialTestValues
            .concat(arrayTestValue)
            .concat(numberTestValue)
            .concat(booleanTestValue);

        it('should return true if not empty stinrg passed', function() {
            expect(Utils.isStringNotEmpty("test-string")).toBeTruthy();
        });

        it('should return false if empty string was passed', function() {
            expect(Utils.isStringNotEmpty('')).toBeFalsy();
        });

        generateTestsWithBadParameters(testValues, 'string', Utils.isStringNotEmpty);
    });

    describe('isNumber()', function() {
        var testValues = specialTestValues
            .concat(arrayTestValue)
            .concat(booleanTestValue);

        it('should return true if a number passed', function() {
            expect(Utils.isNumber(123)).toBeTruthy();
        });

        generateTestsWithBadParameters(testValues, 'number', Utils.isNumber);
    });

    describe('isBoolean()', function() {
        var testValues = specialTestValues
            .concat(arrayTestValue)
            .concat(numberTestValue);

        it('should return true if boolean was passed', function() {
            expect(Utils.isBoolean(true)).toBeTruthy();
        });
        
        generateTestsWithBadParameters(testValues, 'boolean', Utils.isBoolean);
    });
    
    describe('isArrayNotEmpty', function() {
         var testValues = specialTestValues
            .concat(numberTestValue)
            .concat(booleanTestValue);
        
        it('should return true if not empty array was passed', function() {
             expect(Utils.isArrayNotEmpty([123, 321])).toBeTruthy();
        });
        
        it('should return false if empty array was passed', function() {
             expect(Utils.isArrayNotEmpty([])).toBeFalsy();
        });
        
        generateTestsWithBadParameters(testValues, 'array', Utils.isArrayNotEmpty);
    });
});