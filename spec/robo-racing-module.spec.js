describe('RoboRacingModule', function() {
    var musicTrack,
        musicLevel,
        effectsTrack,
        effectsLevel,
        isLoop,
        racingResults;

    beforeAll(function() {
        var firstResult = new RoboRacingModule.Result(1, 100),
            secondResult = new RoboRacingModule.Result(2, 200);
        
        racingResults = [firstResult, secondResult];
        
        setDefaultValues();
        setMarkupForTests();
    });

    afterAll(function() {
        $('.race').remove();
        $('.reset').remove();
        $('.sound').remove();
        $('.car').remove();
        $('.results').remove();
    });

    function setDefaultValues() {
        musicTrack = './sounds/coastal.mp3';
        musicLevel = 0.3;
        effectsTrack = './sounds/start-engine.mp3';
        effectsLevel = 1;
        isLoop = true;
    }

    function setMarkupForTests() {
        $('body')
            .append('<div class="button race"></div>')
            .append('<div class="button reset"></div>')
            .append('<div class="button sound"></div>')
            .append('<div class="results"></div>');

        for (var i = 0; i < 4; i++) {
            var carElement = '<div class="car car-' + (i + 1) + '"></div>',
                resultElement = '<div class="car-' + (i + 1) + '"><p></p></div>';

            $('body').append(carElement);
            $('.results').append(resultElement);
        }
    }

    describe('check interface', function() {

        it('should contain specified methods', function() {
            expect(RoboRacingModule.init).toBeDefined();
            expect(RoboRacingModule.enableControlButtons).toBeDefined();
            expect(RoboRacingModule.disableControlButtons).toBeDefined();
            expect(RoboRacingModule.startRace).toBeDefined();

            expect(RoboRacingModule.reset).toBeDefined();
            expect(RoboRacingModule.calculateResults).toBeDefined();
            expect(RoboRacingModule.processResults).toBeDefined();
            expect(RoboRacingModule.moveTheCar).toBeDefined();

            expect(RoboRacingModule.setResultMessage).toBeDefined();
            expect(RoboRacingModule.generateMessage).toBeDefined();
        });
    });

    describe('init()', function() {

        beforeEach(function() {
            spyOn(RoboRacingModule, 'startRace').and.callThrough();
            spyOn(RoboRacingModule, 'reset').and.callThrough();
            spyOn(SoundModule, 'toggleSound');
            spyOn(SoundModule, 'playSound');

            RoboRacingModule.init();
        });

        afterEach(function() {
            RoboRacingModule.startRace.calls.reset();
            RoboRacingModule.reset.calls.reset();
            SoundModule.toggleSound.calls.reset();
            SoundModule.playSound.calls.reset();

            $('.race').off('click');
            $('.reset').off('click');
            $('.sound').off('click');
        });

        it('should append event hadler to the race button and call startRace() on click', function() {
            $('.race').trigger('click');

            expect(RoboRacingModule.startRace.calls.count()).toEqual(1);
        });

        it('should append event hadler to the reset button and call reset() on click', function() {
            $('.reset').trigger('click');

            expect(RoboRacingModule.reset.calls.count()).toEqual(1);
        });

        it('should append event hadler to the sound button and call toggleSound() on click', function() {
            $('.sound').trigger('click');

            expect(SoundModule.toggleSound.calls.count()).toEqual(1);
        });

        it('should start the background music', function() {
            expect(SoundModule.playSound.calls.count()).toEqual(1);
        });

        it('should run background music with appropriate settings', function() {
            expect(SoundModule.playSound).toHaveBeenCalledWith(musicTrack, musicLevel, isLoop);
        });
    });

    describe('enableControlButtons()', function() {

        it('should set disabled property of control buttons to false', function() {
            RoboRacingModule.enableControlButtons();
            expect($('.button:not(".sound")').prop('disabled')).toBeFalsy();
        });
    });

    describe('disableControlButtons()', function() {

        it('should set disabled property of control buttons to true', function() {
            RoboRacingModule.disableControlButtons();
            expect($('.button:not(".sound")').prop('disabled')).toBeTruthy();
        });
    });

    describe('startRace()', function() {

        beforeEach(function() {
            spyOn(RoboRacingModule, 'reset').and.callThrough();
            spyOn(SoundModule, 'playSound');
            spyOn(RoboRacingModule, 'calculateResults').and.callThrough();
        });

        afterEach(function() {
            RoboRacingModule.reset.calls.reset();
            SoundModule.playSound.calls.reset();
            RoboRacingModule.calculateResults.calls.reset();
        });

        it('should call reset()', function() {
            RoboRacingModule.startRace();
            expect(RoboRacingModule.reset.calls.count()).toEqual(1);
        });

        it('should call SoundModule.playSound()', function() {
            RoboRacingModule.startRace();
            expect(SoundModule.playSound.calls.count()).toEqual(1);
        });

        it('should call SoundModule.playSound() with appropriate parameters', function() {
            isLoop = false;

            RoboRacingModule.startRace();
            expect(SoundModule.playSound).toHaveBeenCalledWith(effectsTrack, effectsLevel, isLoop);
        });

        it('should call SoundModule.playSound()', function() {
            RoboRacingModule.startRace();
            expect(SoundModule.playSound.calls.count()).toEqual(1);
        });

        it('should call calculateResults()', function() {
            RoboRacingModule.startRace();
            expect(RoboRacingModule.calculateResults.calls.count()).toEqual(1);
        });
    });

    describe('reset()', function() {
        beforeEach(function() {
            $('body').append('<div class="results"><p>test-text</p></div>');
        });

        afterEach(function() {
            $('.restuls').remove();
        });

        it('should remove text from restults paragraph', function() {
            RoboRacingModule.reset();
            expect($('.restuls p').text()).toEqual('');
        });
    });

    describe('calculateResults()', function() {
        var resultsList;

        beforeEach(function() {
            RoboRacingModule.calculateResults().then(function(results) {
                resultsList = results;
            });
        });

        it('should return a promise', function() {
            expect(RoboRacingModule.calculateResults().promise()).toBeDefined();
        });

        it('should return racing results list', function() {
            expect(typeof resultsList[0].time).toEqual('number');
        });

        it('should return a list with four results', function() {
            expect(resultsList.length).toEqual(4);
        });

        it('should return a list with results sorted by ascending time', function() {
            expect(resultsList[0].time < resultsList[1].time).toBeTruthy();
        });
    });

    describe('processResults()', function() {

        beforeEach(function() {
            spyOn(RoboRacingModule, 'moveTheCar').and.callFake(function() {
                return $.Deferred().promise();
            });
        });

        afterEach(function() {
            RoboRacingModule.moveTheCar.calls.reset();
        });

        afterAll(function() {
            spyOn(RoboRacingModule, 'moveTheCar').and.callThrough();
        });

        it('should return a promise', function() {
            expect(RoboRacingModule.processResults(racingResults).promise()).toBeDefined();
        });

        it('should call moveTheCar() for each result', function() {
            RoboRacingModule.processResults(racingResults);
            expect(RoboRacingModule.moveTheCar.calls.count()).toEqual(2);
        });

        it('should throw an error if not array is passed as a result list', function() {
            expect(function() {
                RoboRacingModule.processResults(true);
            }).toThrow(new Error('Please specify correct array with results'));
        });

        it('should throw an error if the function was called without parameter', function() {
            expect(function() {
                RoboRacingModule.processResults();
            }).toThrow(new Error('Please specify correct array with results'));
        });

        it('should throw an error if empty array is passed as a result list', function() {
            expect(function() {
                RoboRacingModule.processResults([]);
            }).toThrow(new Error('Please specify correct array with results'));
        });
    });

    describe('moveTheCar()', function() {

        it('should return a promise', function() {
            var index = 1;
            expect(RoboRacingModule.moveTheCar(index, racingResults).promise()).toBeDefined();
        });

        it('should throw an error if not a number is passed as index', function() {
            expect(function() {
                RoboRacingModule.moveTheCar([], racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if index isn`t passed', function() {
            expect(function() {
                RoboRacingModule.moveTheCar(null, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if index aren`t passed', function() {
            expect(function() {
                RoboRacingModule.moveTheCar(1, null);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if a number is passed as a racingResults', function() {
            expect(function() {
                RoboRacingModule.moveTheCar(1, 123);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if the function was called without parameter', function() {
            expect(function() {
                RoboRacingModule.moveTheCar();
            }).toThrow(new Error('Please specify correct parameters'));
        });
    });

    describe('setResultMessage()', function() {
        var index,
            carLabel;

        beforeEach(function() {
            index = 1;
            carLabel = 'car-1';
            spyOn(RoboRacingModule, 'generateMessage').and.callThrough();
        });

        it('should call generateMessage() with given parameters', function() {
            RoboRacingModule.setResultMessage(index, carLabel, racingResults);
            expect(RoboRacingModule.generateMessage).toHaveBeenCalledWith(index, racingResults);
        });

        it('should set result text to the correct paragraph', function() {
            var expectedMessage = 'Finished in second place';
            RoboRacingModule.setResultMessage(index, carLabel, racingResults);
            expect($('.results .' + carLabel + ' p').text()).toContain(expectedMessage);
        });

        it('should throw an error if not a number is passed as an index', function() {
            expect(function() {
                RoboRacingModule.setResultMessage({}, carLabel, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if not a sting is passed as a carLabel', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, {}, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if an empty sting is passed as a carLabel', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, '', racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if index isn`t passed', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(null, carLabel, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if carLabel isn`t passed', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, null, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if racingResults isn`t passed', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, carLabel);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if an empty array is passed as a racingResults', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, carLabel, []);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if not array of Results is passed as a racingResults', function() {
            expect(function() {
                RoboRacingModule.setResultMessage(index, carLabel, [{
                    speed: 100
                }]);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if no parameters are passed', function() {
            expect(function() {
                RoboRacingModule.setResultMessage();
            }).toThrow(new Error('Please specify correct parameters'));
        });
    });

    describe('generateMessage()', function() {

        var index = 1;

        it('should return finish message with given index and results', function() {
            var expectedMessage = "Finished in second place and clocked in at 200 milliseconds!";
            expect(RoboRacingModule.generateMessage(index, racingResults)).toEqual(expectedMessage);
        });

        it('should throw an error if index was not passed', function() {
            expect(function() {
                RoboRacingModule.generateMessage(null, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if racing results were not passed', function() {
            expect(function() {
                RoboRacingModule.generateMessage(index, null);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if not a number was passed as an index', function() {
            expect(function() {
                RoboRacingModule.generateMessage({}, racingResults);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if not an array was passed as racing results', function() {
            expect(function() {
                RoboRacingModule.generateMessage(index, {});
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if an empty array was passed as racing results', function() {
            expect(function() {
                RoboRacingModule.generateMessage(index, []);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if not Results array was passed as racing results', function() {
            expect(function() {
                RoboRacingModule.generateMessage(index, [1, 2, 3]);
            }).toThrow(new Error('Please specify correct parameters'));
        });

        it('should throw an error if the functio was called without any parameters', function() {
            expect(function() {
                RoboRacingModule.generateMessage();
            }).toThrow(new Error('Please specify correct parameters'));
        });
    });
});