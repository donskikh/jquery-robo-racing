describe('SoundModule', function() {
    var audioOriginal,
        audioMock,
        musicTrack,
        musicLevel,
        effectsTrack,
        effectsLevel,
        playSpy,
        pauseSpy,
        isLoop;

    beforeAll(function() {
        setDefaultValues();
        mockAudio();
    });

    afterEach(function() {
        playSpy.calls.reset();
        pauseSpy.calls.reset();
    });

    function setDefaultValues() {
        musicTrack = './sounds/coastal.mp3';
        musicLevel = 0.3;
        effectsTrack = './sounds/start-engine.mp3';
        effectsLevel = 1;
        isLoop = true;
    }

    function mockAudio() {
        audioOriginal = window.Audio;
        playSpy = jasmine.createSpy('playSpy');
        pauseSpy = jasmine.createSpy('pauseSpy');

        audioMock = {
            play: playSpy,
            pause: pauseSpy
        };
        window.Audio = function() {
            return audioMock;
        };
    }


    describe('check interface', function() {

        it('should contain specified methods', function() {
            expect(SoundModule.getIsSoundEnabled).toBeDefined();
            expect(SoundModule.setIsSoundEnabled).toBeDefined();
            expect(SoundModule.toggleSound).toBeDefined();
            expect(SoundModule.playSound).toBeDefined();

        });
    });
    
    describe('getIsSoundEnabled()', function() {
        
        beforeEach(function() {
            SoundModule.setIsSoundEnabled(true);
        });
        
        it('should return isSoundEnabled property', function() {
            expect(SoundModule.getIsSoundEnabled()).toBeTruthy();
        });
    });

    describe('setIsSoundEnabled', function() {
        
        it('should set isSoundEnabled property', function() {
            SoundModule.setIsSoundEnabled(false);
            expect(SoundModule.getIsSoundEnabled()).toBeFalsy();
        });
    });

    describe('toggleSound()', function() {

        beforeEach(function() {
            isLoop = false;
            SoundModule.setIsSoundEnabled(true);
            SoundModule.playSound('test-track.mp3', musicLevel, isLoop);

            playSpy.calls.reset();
            pauseSpy.calls.reset();
        });

        afterEach(function() {
            SoundModule.removeNonLoopedSounds();
            playSpy.calls.reset();
            pauseSpy.calls.reset();
        });

        it('should toggle isSoundEnabled property', function() {
            SoundModule.toggleSound();
            expect(SoundModule.getIsSoundEnabled()).toBeFalsy();
        });

        it('should pause each track if sound is disabled', function() {
            SoundModule.toggleSound();
            expect(pauseSpy.calls.count()).toEqual(1);
        });

        it('should play each track if sound is enabled', function() {
            SoundModule.setIsSoundEnabled(false);
            SoundModule.toggleSound();
            expect(playSpy.calls.count()).toEqual(1);
        });
    });

    describe('playSound()', function() {

        beforeEach(function() {
            SoundModule.setIsSoundEnabled(true);
        });

        afterEach(function() {
            playSpy.calls.reset();
        });

        it('should play a new sound if parameters are correct and sound is enabled', function() {
            SoundModule.playSound(musicTrack, musicLevel, isLoop);
        });

        it('should play a new sound without musicLevel parameter', function() {
            SoundModule.playSound(musicTrack, null, isLoop);
        });

        it('should play a new sound without isLoop parameter', function() {
            SoundModule.playSound(musicTrack, musicLevel);
        });

        it('should play a new sound without musicLevel and isLoop parameters', function() {
            SoundModule.playSound(musicTrack);
        });

        it('shoudl return false if sound is disabled', function() {
            SoundModule.setIsSoundEnabled(false);
            expect(SoundModule.playSound(musicTrack)).toBeFalsy();
        });

        it('should throw an exception if path is empty', function() {
            expect(function() {
                SoundModule.playSound('')
            }).toThrow(new Error('Please specify correct parameter values'));
        });

        it('should throw an exception if musicLevel is not a number', function() {
            expect(function() {
                SoundModule.playSound(musicTrack, 'test', isLoop)
            }).toThrow(new Error('Please specify correct parameter values'));
        });

        it('should throw an exception if isLoop is not a boolean', function() {
            expect(function() {
                SoundModule.playSound(musicTrack, musicLevel, 'test')
            }).toThrow(new Error('Please specify correct parameter values'));
        });
    });
});