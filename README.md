# jQuery Robo Racing

jQuery Robo Racing was written with [jQuery v2.2.4](https://jquery.com) to show some fun possibilities of this library.
* Application's tests are written with [NodeJs v5.10.1](https://nodejs.org/en/), so to be sure everything will work you need at least 4+ version.

Go and win!

## Links
[Demo](http://jquery-robo-racing.donskikh.com)   
[Source Code](https://bitbucket.org/donskikh/jquery-robo-racing)   
[Unit tests](http://jquery-robo-racing.donskikh.com/unit-tests)   
[Documentation](http://jquery-robo-racing.donskikh.com/documentation)   
[Portfolio](http://www.donskikh.com)

## Run
Just run two commands in shell:   
```npm install```   
and   
```gulp serve```   
It will run local server accessible at
http://localhost:8080 with live-reload ability.

## Get production-ready version
```gulp build --production```   
The result will be placed in the ```./build``` directory.


## Develop
All source code available at the ```./src``` directory.

### Unit tests
Are written with [Jasmine](http://jasmine.github.io) and work with [Karma](https://karma-runner.github.io/0.13/index.html) test-runner.   
Placed at the ```./spec``` directory.
You can easily run them with:   
```gulp test```   

### Documentation
Documentation in html will be available after   
```gulp build --production``` or   
```gulp documentation``` command in the   
```./html-documentation``` directory.

### Gulp
* directory: ```./gulp-tasks```

The next gulp tasks are available in the application:

```gulp build``` (prepare for develop|production; run tests; create documentation. Output: ./build)   
```gulp clean``` (remove the ./build directory)   
```gulp default``` (run build)   
```gulp assets``` (inject scripts/styles, minify if production flag was set; copy and optimize images and sounds)   
```gulp images``` (optimize images and copy to the ./build directory)   
```gulp scripts``` (copy to the ./build directory - used with watch task)   
```gulp serve``` (run local server with live-reload)   
```gulp styles``` (copy to the ./build directory - used with watch task)   
```gulp test``` (run unit test)   
```gulp watch``` (watch on html, css, js files)   
```gulp documentation``` (create documentation for the project: ./html-documentation)   
```gulp eslit``` (check js code for style problems)   

* Default gulp task: ```gulp``` will run build task.
* Every task could be run with the ```--production``` flag.
* Unit tests could be run with the ```--debug``` flag. It will run them with Chrome instead of PhantomJs, and switch Karma's ```singleRun``` parameter to false to allow you to debug everything within browser's  console.

## Licence
The MIT License (MIT)
Copyright (c) 2016 Donskikh A.V.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.