/* global Utils, SoundModule, $ */
'use strict';

/** @namespace RoboRacingModule */
var RoboRacingModule = (function($) {
    var finishPlaceNames = ['first', 'second', 'third', 'fourth'],
        maxTime = 10000, //in milliseconds
        finishLine = '900px',
        backgroundTrack = './sounds/coastal.mp3',
        effectsTrack = './sounds/start-engine.mp3',
        musicLevel = 0.3,
        effectsLevel = 1;

    /**
     * Bind event handlers, start background music
     * @memberof RoboRacingModule
     */
    function init() {
        var isLoop = true,
            that = this;

        $('.race').on('click', $.proxy(that.startRace, that));
        $('.reset').on('click', $.proxy(that.reset, that));
        $('.sound').on('click', function() {
            SoundModule.toggleSound();
            $('.sound').toggleClass('disabled');
        });
        SoundModule.playSound(backgroundTrack, musicLevel, isLoop);
    }

    /**
     * Run race
     * @memberof RoboRacingModule
     */
    function startRace() {
        var that = this,
            isLoop = false;

        that.reset();
        SoundModule.playSound(effectsTrack, effectsLevel, isLoop);

        that.calculateResults()
            .then(function(racingResults) {
                that.disableControlButtons();
                return that.processResults(racingResults);

            }).then(that.enableControlButtons);
    }

    /**
     * Enable control buttons
     * @memberof RoboRacingModule
     * @function enableControlButtons
     */
    function enableControlButtons() {
        $('.button:not(".sound")').prop('disabled', false);
    }

    /**
     * Disable control buttons
     * @memberof RoboRacingModule
     */
    function disableControlButtons() {
        $('.button:not(".sound")').prop('disabled', true);
    }

    /**
     * Drop to the initial state (cars, results)
     * @memberof RoboRacingModule
     */
    function reset() {
        $('.results p').text('');
        $('.car').css('left', '0px');
    }

    /**
     * Calculate results and run processing
     * @memberof RoboRacingModule
     */
    function calculateResults() {
        var deferred = $.Deferred();
        var racingResults = [];

        for (var i = 1; i <= $('.car').length; i++) {
            var time = Math.ceil(Math.random() * maxTime);
            var result = new Result(i, time);
            racingResults.push(result);
        }

        racingResults.sort(function(a, b) {
            return a.time - b.time;
        });

        deferred.resolve(racingResults);

        return deferred.promise();
    }

    /**
     * Move each car to the finish line
     * @memberof RoboRacingModule
     * @throws {Error} If not array or empty array passed
     * @param {Array.<Result>} racingResults Results list
     */
    function processResults(racingResults) {
        if (!isArrayWithRacingResults(racingResults)) {
            throw new Error('Please specify correct array with results');
        }
        var deferred = $.Deferred(),
            that = this;

        for (var i = 0; i < racingResults.length; i++) {
            that.moveTheCar(i, racingResults).then(function(resultIndex, label) {
                RoboRacingModule.setResultMessage(resultIndex, label, racingResults);
            });
        }

        $('.car').promise().done(function() {
            deferred.resolve();
        });

        return deferred.promise();
    }

    /**
     * Move the car and publish result message
     * @memberof RoboRacingModule
     * @throws {Error} If not a number passed
     * @param {number} index Index to get the car and generate message
     * @param {Array.<Result>} racingResults array with racing results
     */
    function moveTheCar(index, racingResults) {
        if (!Utils.isNumber(index)
            || !isArrayWithRacingResults(racingResults)) {
            throw new Error('Please specify correct parameters');
        }

        var resultCar = racingResults[index],
            deferred = $.Deferred();

        $('.' + resultCar.carLabel).animate({
            left: finishLine
        }, resultCar.time, function() {
            deferred.resolve(index, $(this).data('label'));
        });

        return deferred.promise();
    }

    /**
     * Set result message to the dashboard
     * @memberof RoboRacingModule
     * @throws {Error} If incorrect parameters passed
     * @param {number} index result index
     * @param {string} carLabel car label
     * @param {Array.<Result>} racingResults Racing results list
     */
    function setResultMessage(index, carLabel, racingResults) {
        var that = this;

        if (!Utils.isNumber(index)
            || !Utils.isStringNotEmpty(carLabel)
            || !isArrayWithRacingResults(racingResults)) {
            throw new Error('Please specify correct parameters');
        }
        var message = that.generateMessage(index, racingResults);
        $('.results').find('.' + carLabel + ' p').text(message);
    }

    /**
     * Generate result message
     * @memberof RoboRacingModule
     * @throws {Error} If incorrect parameters passed
     * @param   {number} index Index to generate the correct message
     * @return {string} result message
     * @param {Array.<Result>} racingResults Racing results list
     */
    function generateMessage(index, racingResults) {
        if (!Utils.isNumber(index)
            || !isArrayWithRacingResults(racingResults)) {
            throw new Error('Please specify correct parameters');
        }

        return 'Finished in '
            + finishPlaceNames[index]
            + ' place and clocked in at '
            + racingResults[index].time
            + ' milliseconds!';
    }

    /**
     * Race Result Class
     * @memberof RoboRacingModule
     * @constructor
     * @param {number} carNumber Number of the car
     * @param {number} time     It's time
     */
    function Result(carNumber, time) {
        this.carLabel = 'car-' + (carNumber || 0);
        this.time = time || 0;
    }

    /**
     * Check whether the passed array is not empty and contains racing results
     * @param racingResults
     * @returns {boolean}
     * @private
     */
    function isArrayWithRacingResults(racingResults) {
        return Utils.isArrayNotEmpty(racingResults)
            && racingResults[0] instanceof RoboRacingModule.Result;
    }

    return {
        init: init,
        enableControlButtons: enableControlButtons,
        disableControlButtons: disableControlButtons,
        startRace: startRace,
        reset: reset,
        calculateResults: calculateResults,
        processResults: processResults,
        moveTheCar: moveTheCar,
        generateMessage: generateMessage,
        setResultMessage: setResultMessage,
        Result: Result
    }
})($);