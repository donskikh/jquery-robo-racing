/* global Utils */
'use strict';

/** @namespace SoundModule */
var SoundModule = (function() {
    var options = {
            isSoundEnabled: true,
            soundLevel: 1
        },
        sounds = [];

    /**
     * Get is sound enabled property
     * @memberof SoundModule
     * @returns {boolean} enabled | disabled
     */
    function getIsSoundEnabled() {
        return options.isSoundEnabled;
    }

    /**
     * Set is sound enabled property
     * @memberof SoundModule
     * @throws {Error} If passed value isn't a boolean
     * @param {boolean} value New value
     */
    function setIsSoundEnabled(value) {
        if (typeof value !== 'boolean') {
            throw new Error('Please specify boolean value');
        }
        options.isSoundEnabled = value;
    }

    /**
     * Toggle sound
     * @memberof SoundModule
     */
    function toggleSound() {
        options.isSoundEnabled = !options.isSoundEnabled;

        sounds.forEach(function(sound) {
            options.isSoundEnabled ? sound.play() : sound.pause();
        });
    }

    /**
     * Play sound
     * @memberof SoundModule
     * @throws {Error} If incorrect parameters passed
     * @param {string} path Path to sound
     * @param {number} [soundLevel] From 0.0 to 1.0
     * @param {boolean} [isSoundLoop] [isLoop=false] Play in loop or not.
     */
    function playSound(path, soundLevel, isSoundLoop) {
        var volume = soundLevel || options.soundLevel,
            isLoop = isSoundLoop || false,
            that = this;

        if (!Utils.isStringNotEmpty(path) || !Utils.isNumber(volume) || !Utils.isBoolean(isLoop)) {
            throw new Error('Please specify correct parameter values');
        }

        if (!options.isSoundEnabled || !window.Audio) {
            return false;
        }
        var sound = new Audio(path);
        sound.volume = volume;
        sound.loop = isLoop;
        sound.play();
        sounds.push(sound);

        if (!isLoop) {
            sound.onended = function() {
                that.removeNonLoopedSounds();
            }
        }
    }

    /**
     * Remove all non-looped sounds
     * @memberof SoundModule
     * @private
     */
    function removeNonLoopedSounds() {
        sounds = sounds.filter(function(sound) {
            return sound.loop === true;
        });
    }

    return {
        getIsSoundEnabled: getIsSoundEnabled,
        setIsSoundEnabled: setIsSoundEnabled,
        removeNonLoopedSounds: removeNonLoopedSounds,
        toggleSound: toggleSound,
        playSound: playSound
    }
})();