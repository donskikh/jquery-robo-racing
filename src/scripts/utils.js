'use strict';

/** @namespace Utils */
var Utils = (function() {
    /**
     * Check whether the passed value is a string and it's not empty
     * @memberof Utils
     * @param {string} value Value to check
     */
    function isStringNotEmpty(value) {
        return typeof value === 'string' && value !== '';
    }

    /**
     * Check whether the passed value is a number
     * @memberof Utils
     * @param {number} value Value to check
     */
    function isNumber(value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    /**
     * Check whether the passed value is a boolean
     * @memberof Utils
     * @param {boolean} value Value to check
     */
    function isBoolean(value) {
        return typeof value === 'boolean';
    }

    /**
     * Check whether the passed value is an array and it's not empty
     * @memberof Utils
     * @param {Array} value Array to check
     */
    function isArrayNotEmpty(value) {
        return value instanceof Array && value.length > 0;
    }

    return {
        isStringNotEmpty: isStringNotEmpty,
        isNumber: isNumber,
        isBoolean: isBoolean,
        isArrayNotEmpty: isArrayNotEmpty
    }
})();